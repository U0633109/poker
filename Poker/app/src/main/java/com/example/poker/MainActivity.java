package com.example.poker;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageButton P1,P2,P3,P4,P5,play1,play2;
    Button Callm,Fold,Raise,show;
    TextView pcAll,All,playerAll,etMoney,pcm,playerm;
    ArrayList<Card> f = new ArrayList<>();
    ArrayList<Card> player,pc,moster,playMax,pcMax,pcpart;
    int[] cm;
    boolean first = false;
    int n = 52;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Callm=findViewById(R.id.Callm);
        setVariable();
        poker(f);
        moster = new ArrayList<>();
        deal(moster,f,5);
        player = new ArrayList<>();
        deal(player,f,2);
        pc = new ArrayList<>();
        deal(player,f,2);
//        pcpart=new ArrayList<>();

        final String play = "p" + player.get(0).toString();
        showP(play1,play);

        String play0 = "p" + player.get(1).toString();
        showP(play2,play0);

        playMax = new ArrayList<>();
        pcMax = new ArrayList<>();

        P1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p = "p" + moster.get(0).toString();
                showP(P1,p);
                player.add(moster.get(0));
                pc.add(moster.get(0));

                String p2 = "p" + moster.get(1).toString();
                showP(P2,p2);
                player.add(moster.get(1));
                pc.add(moster.get(1));

                String p3 = "p" + moster.get(2).toString();
                showP(P3,p3);
                player.add(moster.get(2));
                pc.add(moster.get(2));
            }
        });

        P4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p = "p" + moster.get(3).toString();
                showP(P4,p);
                player.add(moster.get(3));
                pc.add(moster.get(3));
            }
        });

        P5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p = "p" + moster.get(4).toString();
                showP(P5,p);
                player.add(moster.get(4));
                pc.add(moster.get(4));
            }
        });


        Callm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int sum = Integer.parseInt(playerm.getText().toString())-10; //電腦
                playerm.setText(String.valueOf(sum));
                int add = Integer.parseInt(playerAll.getText().toString())+10;
                playerAll.setText(String.valueOf(add));
            }
        });

        Fold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        Raise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String m = etMoney.getText().toString();
                int sum = Integer.parseInt(playerm.getText().toString())-Integer.parseInt(m);
                playerm.setText(String.valueOf(sum));
                int add = Integer.parseInt(playerAll.getText().toString())+Integer.parseInt(m);
                playerAll.setText(String.valueOf(add));
                int x=iscomCallm(pc);
                if(x==-1){

                }else if(x==0){
                    pcAll.setText(add);
                }else {
                    Toast toast = Toast.makeText(MainActivity.this,
                            "電腦加注"+(x*10), Toast.LENGTH_LONG);
                    Raise.setEnabled(false);
                    add =  Integer.parseInt(pcAll.getText().toString())+Integer.parseInt(m);
                }
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playcard(player,playMax);
                //Log.e("0",pc.toString());
//                playcard(pc,pcMax);
                ArrayList<Card> win = new ArrayList<>(playMax);

                String p = "p" + win.get(0).toString();
                showP(P1,p);

                String p2 = "p" + win.get(1).toString();
                showP(P2,p2);

                String p3 = "p" + win.get(2).toString();
                showP(P3,p3);

                String p4 = "p" + win.get(3).toString();
                showP(P4,p4);

                String p5 = "p" + win.get(4).toString();
                showP(P5,p5);
            }
        });

    }

    void setVariable(){
        P1 = findViewById(R.id.P1);
        P2 = findViewById(R.id.P2);
        P3 = findViewById(R.id.P3);
        P4 = findViewById(R.id.P4);
        P5 = findViewById(R.id.P5);
        play1 = findViewById(R.id.play1);
        play2 = findViewById(R.id.play2);
        Callm = findViewById(R.id.Callm);
        Fold = findViewById(R.id.Fold);
        Raise = findViewById(R.id.Raise);
        pcAll = findViewById(R.id.pcAll);
        All = findViewById(R.id.All);
        playerAll = findViewById(R.id.playerAll);
        etMoney = findViewById(R.id.etMoney);
        pcm = findViewById(R.id.pcm);
        playerm = findViewById(R.id.playerm);
        show = findViewById(R.id.show);
    }

    class Card implements Comparable<Card>{  //卡
        public String flush;
        public int number;

        public Card(String flush, int number) {
            this.flush = flush;
            this.number = number;
        }
        @Override
        public String toString() {
            return flush + number;
        }
        @Override
        public int compareTo(Card a) {
            return this.number - a.number;
        }
    }

    void poker(ArrayList<Card> f){        //52張牌

        for (int i = 0; i < 4; i++){
            String type="";
            switch (i){
                case 0:
                    type = "s";
                    break;
                case 1:
                    type = "h";
                    break;
                case 2:
                    type = "d";
                    break;
                case 3:
                    type = "c";
                    break;
            }
            for (int j = 1; j < 14; j++){
                Card s = new Card(type,j);
                f.add(s);
            }
        }
    }

    void deal(ArrayList<Card> a,ArrayList<Card> f,int t){ //發牌
        Random r = new Random();
        for (int i = 0; i < t; i++){
            int b = r.nextInt(n);
            a.add(f.get(b));
            f.remove(f.get(b));
            n--;
        }
    }

    void showP(ImageButton P,String p){
        String uri = "@drawable/" + p;
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());
        Drawable image = getResources().getDrawable(imageResource);
        P.setImageDrawable(image);
    }

    int proportion(String a){
        switch (a){
            case "散排":
                return 1;
            case "一對":
                return 2;
            case "兩對":
                return 3;
            case "三條":
                return 4;
            case "順子":
                return 5;
            case "同花":
                return 6;
            case "葫蘆":
                return 7;
            case "鐵支":
                return 8;
            case "同花順":
                return 9;
        }
        return 1;
    }

    ArrayList<Card> pokerSize5(ArrayList<Card> a, ArrayList<Card> b){
        Collections.sort(a);
        Collections.sort(b);

        if(cardtype5(a).equals(cardtype5(b))){

            if( proportion(cardtype5(a)) == 1 ){            //散排
                int amax = 0;
                int bmax = 0;

                if(a.get(0).number == 1){
                    amax = 14;
                }else {
                    amax = a.get(4).number;
                }

                if(b.get(0).number == 1){
                    bmax = 14;
                }else {
                    bmax = b.get(4).number;
                }

                if(amax > bmax){
                    return a;
                }else {
                    if(amax < bmax){
                        return b;
                    }
                }
            }

            if( proportion(cardtype5(a)) == 2 || proportion(cardtype5(a)) == 3 || proportion(cardtype5(a)) == 4
                    || proportion(cardtype5(a)) == 7 || proportion(cardtype5(a)) == 8 ) {
                int max = 0;
                int k = 0;
                int amax = 0;
                int bmax = 0;
                int w = 0;

                for (int j = 0; j < 5; j++) {
                    for (int i = 0; i < 5; i++) {
                        if (a.get(j).number == a.get(i).number) {
                            k++;
                        }
                    }
                    if (k > max) {
                        max = k;
                        amax = a.get(j).number;
                    }
                    k = 0;
                }

                for (int j = 0; j < 5; j++) {                         //一對 二對 三條 葫蘆 鐵支
                    for (int i = 0; i < 5; i++) {
                        if (b.get(j).number == b.get(i).number) {
                            k++;
                        }
                    }
                    if (k > max) {
                        max = k;
                        bmax = b.get(j).number;
                    }
                    if(k == 2 && max == 2){
                        w = b.get(j).number;
                    }
                    k = 0;
                }
                if(w != 0){
                    if(w == 1){
                        w = 14;
                    }
                    if(bmax == 1){
                        bmax = 14;
                    }
                    int s = bmax - w;
                    if(s>0){
                        bmax = s;
                    }else {
                        bmax = -s;
                    }
                }

                if (amax > bmax) {
                    return a;
                } else {
                    if (amax < bmax) {
                        return b;
                    }
                }
            }

            if( proportion(cardtype5(a)) == 5 ){   //順子
                int amax;
                int bmax;

                if(a.get(0).number == 1 && a.get(4).number == 13){
                    amax = 14;
                }else{
                    amax = a.get(4).number;
                }

                if(b.get(0).number == 1 && b.get(4).number == 13){
                    bmax = 14;
                }else{
                    bmax = b.get(4).number;
                }

                if (amax > bmax) {
                    return a;
                } else {
                    if (amax < bmax) {
                        return b;
                    }
                }
            }

            if( proportion(cardtype5(a)) == 6 ) {   //同花
                int amax = 0;
                int bmax = 0;

                switch (a.get(0).flush){
                    case "s":
                        amax = 4;
                        break;
                    case "h":
                        amax = 3;
                        break;
                    case "d":
                        amax = 2;
                        break;
                    case "c":
                        amax = 1;
                        break;
                }
                switch (b.get(0).flush){
                    case "s":
                        bmax = 4;
                        break;
                    case "h":
                        bmax = 3;
                        break;
                    case "d":
                        bmax = 2;
                        break;
                    case "c":
                        bmax = 1;
                        break;
                }
                if (amax > bmax) {
                    return a;
                } else {
                    if (amax < bmax) {
                        return b;
                    }
                }
            }
        }else{
            int s = proportion(cardtype5(a))-proportion(cardtype5(b));
            if(s > 0) {
                return a;
            }else {
                return b;
            }
        }
        return a;
    }

    String cardtype5(ArrayList<Card> a){      //五張牌型
        Collections.sort(a);
        int f = 0; //花色
        int s = 0; //連續
        int k = 0; //數字一樣


        for(int i = 0; i < 5; i++){
            if(a.get(0).flush.equals(a.get(i).flush)){
                f++;
            }
        }

        for(int i = 0; i < 5; i++){
            if(a.get(i).number == a.get(0).number + i){
                s++;
            }
        }

        if(f == 5 && s == 5){
            return "同花順";
        }else{
            if(s == 5){
                return "順子";
            }else {
                if(f == 5){
                    return "同花";
                }
            }
        }


        int m = 0;
        int n = 0;
        int w = 0;
        for(int j = 0; j < 5; j++){
            for(int i = 0; i < 5; i++) {
                if (a.get(j).number == a.get(i).number) {
                    k++;
                }
            }
            if(k == 2)w++;
            if(w == 4)n = 2;
            if(w == 2 && m == 3)n = 2;
            if(k > m){
                m = k;
            }
            k = 0;
        }
        switch (m) {
            case 4:
                return "鐵支";
            case 3:
                if(n == 2){
                    return "葫蘆";
                }else{
                    return "三條";
                }
            case 2:
                if(m == n){
                    return "兩對";
                }else {
                    return "一對";
                }
            case 1:
                return "散排";
        }
        return "散排";
    }

    void ts(int x,int n,int m,ArrayList<Card> c,ArrayList<Card> max){
        ArrayList<Card> list = new ArrayList<>();

        for(int i = cm[x - 1] + 1; i <= (n - m + x); i++){
            cm[x] = i;
            if(x == (m - 1)){
                for(int j = 0; j < m; j++){
                    Card s = new Card(c.get(cm[j]).flush,c.get(cm[j]).number);
                    list.add(s);
                    System.out.print(c.get(cm[j])+ ",");
                }

                if(!first){
                    for (Card copy : list) {
                        max.add(copy);
                    }
                    first = true;
                }

                if(proportion(cardtype5(list)) == proportion(cardtype5(max))){
                    ArrayList<Card> Max = new ArrayList<>(pokerSize5(list,max));
                    max.clear();
                    for (Card copy : Max) {
                        max.add(copy);
                    }
                }else {
                    if(proportion(cardtype5(list)) > proportion(cardtype5(max))){
                        max.clear();
                        for (Card copy : list) {
                            max.add(copy);
                        }
                    }
                }

                System.out.print(cardtype5(list));

                list.clear();

                System.out.print("\n");
            }else{
                ts(x+1,n,m,c,max);
            }
        }
    }

    void playcard(ArrayList<Card> c,ArrayList<Card> max){
        int n = c.size();

        cm = new int[5];                    //C7取5
        for(int i = 0; i <= (n - 5); i++){
            cm[0] = i;
            ts(1,n,5,c,max);
        }
    }
    void comCallm(){

    }
    void comRaise(){

    }
    int iscomCallm(ArrayList<Card> a){
        int [] data=new int [14];//數字
        int [] data2=new int [4];
        // int [][] card=new int [14][4];//0 梅花 1 菱形 2 愛心 3 黑桃
        boolean four=false,three=false,two=false;
        int fourCardNum;
        int []threeCardNum=new int [2];//最多只能湊出兩個三條
        int []twoCardNum=new int [3];//最多只能湊出三個二對=
        int times_3=0,times_2=0;//三條數量 對子數量
        int w=0;//意願度
        for(int i=0;i<a.size();i++) {
            data[a.get(i).number]++;
            //花色
            switch (a.get(i).flush){
                case "♠":
                    data2[0]++;
                    break;
                case "♥":
                    data2[1]++;
                    break;
                case "♦":
                    data2[2]++;
                    break;
                case "♣":
                    data2[3]++;
                    break;

            }

            if(data[a.get(i).number]==4) {
                four=true;
                fourCardNum=a.get(i).number;
                times_3--;
                threeCardNum[times_3]=0;
                if(times_3==0)three=false;
            }else if(data[a.get(i).number]==3) {
                three=true;
                threeCardNum[times_3]=a.get(i).number;
                times_3++;
                times_2--;
                twoCardNum[times_2]=0;
                if(times_2==0)two=false;
            }else if (data[a.get(i).number]==2) {
                two=true;
                twoCardNum[times_2]=a.get(i).number;
                times_2++;
            }
        }
        //順子
        // 100同花大順（皇家同花順，Royal Straight Flush）
        // 97同花順（Straight Flush）1
        // 95四條（鐵支，Four of a kind）1
        // 90葫蘆（滿堂紅，Full house）2
        // 85同花（Flush）
        // 80順子（Straight）
        // 75三條（Three of a kind）
        // 70兩對（Two pair）
        // 60一對（One pair）
        // 60↓散牌（高牌, High card，No-pair，Zitch）
        while(true){
            if(four) {
                //鐵支
                w=95;
                break;
            }else if(two&&three) {
                //葫蘆
                w=90;
                break;
            }
            //桐花
            for(int i=0;i<4;i++){
                if(data2[i]>4){
                    w=85;
                    break;
                }
            }
            if(w!=0)break;
            int st=1,end=1,r=0,ma=0;
            for(int i=1;i<13;i++) {
                if(data[i]!=0) {r++;end++;}
                else {r=0;st=i+1;end=i+1;}
                if(ma<r)ma=r;
            }
            if(ma==5){
                //順子
                w=80;
                break;
            }else if(three) {
                //只剩三條
                w=75;
                break;
            }else if(two) {
                //只剩對子
                if(times_2>1){
                    //兩對子
                    w=70;
                    break;
                }else {
                    //一對子
                    w=60;
                    break;
                }
            }else{
                //以下皆為雜牌
                if(ma==3){
                    //有連續的三張牌
                    w=50;
                    break;
                }else if (ma==4){
                    //有連續的四張牌
                    w=55;
                    break;
                }else {
                    //沒有任何的排組
                    w=45;
                    break;
                }
            }
        }
        Random ran = new Random();
        int r=ran.nextInt(100)+1;
        if(r>w)return -1;//棄排
        else {
            r=ran.nextInt(100)+1;
            int x=w-r;
            if(r>w)return 0; //不跟注
            else if(x<10)return 1; //0棄排 1流排
            else if(x<20)return 2;
            else if(x<30)return 3;
            else if(x<40)return 4;
            else if(x<50)return 5;
            else return 6;
        }
    }


}
